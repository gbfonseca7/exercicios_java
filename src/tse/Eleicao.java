/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tse;

import java.util.Scanner;

/**
 *
 * @author gabriel
 */
public class Eleicao {
    static Scanner tecla = new Scanner(System.in);
    static Candidato[] candidatos = new Candidato[20];
    static int index = 0;
    static double votos = 0 ;
    
    public static void main(String[] args){
        
        int op;
        
        
        do{
            System.out.println("\tBem Vindo ao TSE");
            System.out.println("1 - Para Candidatar-se");
            System.out.println("2 - Para Votar");
            System.out.println("3 - Listar Candidatos");
            System.out.println("4 - Encerrar Eleição");

            System.out.println("Digite a opção desejada: ");
            op = tecla.nextInt();
        
            switch(op){
                case 1: candidatar_se(); break;
                case 2: votar(); break;
                case 3: listCand(); break;
                case 4: encerrarEleicao(); break;
            }
        
        } while(op != 4);
    }
    
    public static void candidatar_se(){
        System.out.println("Escreva seu nome: ");
        String nome = tecla.next();
        System.out.println("Escreva seu número de Eleição: ");
        int num = tecla.nextInt();
        
        candidatos[index++] = new Candidato(num, nome);
    }
    
    public static void votar(){
        System.out.println("Insira o número do Candidato em que deseja Votar: ");
        int candVotar = tecla.nextInt();
        votos++;
        for(int i = 0; i < candidatos.length - 1; i++){
            if(candidatos[i] != null){
                if(candidatos[i].candNum == candVotar){
                    candidatos[i].votar();
                }
            }
        }
    }
    
    public static void listCand(){
        System.out.println("Número Eleitoral: ......... - Nome: .........");
        for(int i = 0; i < candidatos.length - 1; i++){
            if(candidatos[i] != null){
                System.out.println(candidatos[i].candNum + " - " + candidatos[i].getCandNome());
            }
        }
    }
    
    public static void encerrarEleicao(){
        int ganhador = 0;
        for(int i = 0; i < candidatos.length - 1; i++){
            if(candidatos[i] != null){
                if(candidatos[i].getVotos() > ganhador){
                    ganhador = candidatos[i].getVotos();
                }
            }
        }
        
        for(int i = 0; i < candidatos.length - 1; i++){
            if(candidatos[i] != null){
                if(ganhador == candidatos[i].getVotos()){
                    double resultado = candidatos[i].getVotos() / votos;
                    resultado *= 100;
                    System.out.println("Vencedor da Eleição Foi: ");
                    System.out.printf("%s Venceu com %.2f %% dos Votos!\n", candidatos[i].getCandNome(), resultado);
                }
            }
        }
    }
}
