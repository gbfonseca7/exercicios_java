/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tse;

/**
 *
 * @author gabriel
 */
public class Candidato {
    public final int candNum;
    private final String candNome;
    private int candVotos;
    
    public void votar(){
        this.candVotos += 1;
    }
    
    public Candidato(int candNum, String candNome){
        this.candNum = candNum;
        this.candNome = candNome;
        this.candVotos = 0;
    }
    
    public String getCandNome(){
        return this.candNome;
    }
    
    public int getVotos(){
        return this.candVotos;
    }
    
}
