package app;

import java.util.Scanner;


public class ObjectArray {
    
    static Pessoa[] lista = new Pessoa[100];
    static int index = 0;
    static Scanner tecla = new Scanner(System.in);
    
        

    public static void main(String[] args) {
        int in;
        
        do{
            System.out.println("\nSeja bem-Vindo! -- Digite o Comando desejado");
            System.out.println("1 - Inserir uma pessoa na Lista");
            System.out.println("2 - Listar as pessoas da Lista");
            System.out.println("3 - Sair");
            
            System.out.println("Digite a operação desejada: ");
            in = tecla.nextInt();
            
            switch(in){
                case 1: inserirPessoa(); break; 
                case 2: listarPessoas(); break;
                case 3: break;
            }
            
        } while(in != 3);
        
        
    }
    
    public static void inserirPessoa(){
        System.out.println("\nDigite o Nome da Pessoa: ");
        String name;
        name = tecla.next();
        lista[index++] = new Pessoa(name);
        System.out.println("Inserido com Sucesso!");
    }
    
    public static void listarPessoas(){
        System.out.println("-----Pessoas na Lista-----");
        System.out.println("Nome: ...........");
        for(int i = 0; i < lista.length-1; i++){
            if(lista[i] == null){
                break;
            } else {
                System.out.println(i + " - Nome: " + lista[i].nome);
            }
        }
    }
    
}
