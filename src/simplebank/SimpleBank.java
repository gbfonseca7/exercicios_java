/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simplebank;

import java.util.Scanner;
import java.util.ArrayList;
/**
 *
 * @author gabriel
 */
public class SimpleBank {

    static Account[] lista = new Account[100];
    static int index = 0;
    static Scanner tecla = new Scanner(System.in);
    
    public static void main(String[] args) {
        
        int op;
        
        do {
            
            System.out.println("\n\t-----Seja Bem-vindo ao Bank-----\n");
            System.out.println("1 - Inserir Conta");
            System.out.println("2 - Excluir Conta");
            System.out.println("3 - Débito ou Crédido");
            System.out.println("4 - Consultar Saldo");
            System.out.println("5 - Transferência");
            System.out.println("6 - Sair");
            
            System.out.println("\nSelecione a Opção Desejada: "); 
            op = tecla.nextInt();
            
            switch(op){
                case 1: inserirConta(); break;
                case 2: excluirConta(); break;
                case 3: debitoCredito(); break;
                case 4: consultaSaldo(); break;
                case 5: transferencia(); break;
                case 6: break;
               
            }
            
        } while(op != 6);
        
    }
    
    public static void inserirConta(){
        System.out.println("Digite o Número da Conta: ");
        int num = tecla.nextInt();
        System.out.println("Digite o Saldo da Conta: ");
        int saldo = tecla.nextInt();
        lista[index++] = new Account(num, saldo);
        System.out.println("Conta Criada com Sucesso!");
    }
    
    public static void excluirConta(){
        System.out.println("Digite o Número da Conta em que deseja excluir: ");
        int numConta = tecla.nextInt();
        for(int i = 0; i < lista.length - 1; i++){
            if(lista[i] != null){
                if(lista[i].contaNum == numConta){
                    lista[i] = null;
                }
            }
        }
    }
    
    public static void debitoCredito(){
        System.out.println("Digite 1 para Débito ou 2 para Crédito: ");
        int op = tecla.nextInt();
        switch(op){
            case 1: break;
            case 2: break;
        }
    }
    
    public static void consultaSaldo(){
        
        System.out.println("\nDigite o Número da Sua Conta: ");
        int contaNum = tecla.nextInt();
        for(int i = 0; i < lista.length - 1; i++){
            if(lista[i] != null){
                if(lista[i].contaNum == contaNum){
                    System.out.println("\nConta: " + lista[i].contaNum + "\nSaldo: " + lista[i].saldo);
                }
            }
        }
        
    }
    
    public static void transferencia(){
        System.out.println("Digite o número da Conta: ");
        int remetConta = tecla.nextInt();
        System.out.println("O valor desejado de Transferência: ");
        int remetValor = tecla.nextInt();
        System.out.println("Digite o número da Conta destinatária: ");
        int destConta = tecla.nextInt();
        
        for(int i = 0; i < lista.length - 1; i++){
            if(lista[i] != null){
                if(lista[i].contaNum == remetConta){
                    lista[i].saldo -= remetValor;
                }
                if(lista[i].contaNum == destConta){
                    lista[i].saldo += remetValor;
                }
            }
        }
    }
    
}
