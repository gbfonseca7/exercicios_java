/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estoque;

import java.util.Scanner;

/**
 *
 * @author gabriel
 */
public class Estoque {
    static Produto[] produtos = new Produto[100];
    static int index = 0;
    static Scanner tecla = new Scanner(System.in);
    
    public static void main(String[] args){
        int op;
        do{
            System.out.println("\n\t Estoque");
            System.out.println("1 - Estocar um Produto");
            System.out.println("2 - Verificar Estoque");
            System.out.println("3 - Retirar Produto");
            System.out.println("4 - Listar Saldo do Estoque");
            System.out.println("5 - Sair");
            
            System.out.println("Digte a Operação desejada: ");
            op = tecla.nextInt();
            
            switch(op){
                case 1: estocarProd(); break;
                case 2: verificarEstoque(); break;
                case 3: retirarProd(); break;
                case 4: listarSaldo(); break;
                case 5: break;
            }
        } while(op != 5);
    }
    
    public static void estocarProd(){
        System.out.println("Digite o nome do Produto: ");
        String nome = tecla.next();
        System.out.println("Preço do Produto: ");
        double preco = tecla.nextDouble();
        System.out.println("Quantidade em que irá Estocar: ");
        int qnt = tecla.nextInt();
        produtos[index++] = new Produto(nome, preco, qnt);
    }
    
    public static void verificarEstoque(){
        for(int i = 0; i < produtos.length - 1; i++){
            if(produtos[i] != null){
                if(produtos[i].quantidade == 0){
                    System.out.println(produtos[i].nome + " Precisa ser reposto.");
                }
            }
        }
    }
    
    public static void retirarProd(){
        System.out.println("\nDigite o nome do Produto em que deseja retirar: ");
        String prod = tecla.next();
        
        for(int i = 0; i < produtos.length - 1; i++){
            if(produtos[i] != null){
                if(produtos[i].nome.equals(prod)){
                    if(produtos[i].quantidade > 0){
                        produtos[i].quantidade -= 1;
                    }
                    else {
                        System.out.println("\nProduto fora de Estoque.");
                    }
                }
            }
        }
    }
    
    public static void listarSaldo(){
        double saldo = 0;
        for(int i = 0; i < produtos.length - 1; i++){
            if(produtos[i] != null){
                saldo += produtos[i].preco;
            }
        }
        System.out.println("\nO saldo do Estoque é R$" + saldo);
    }
}
