/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package academico;

import java.util.Scanner;

/**
 *
 * @author gabriel
 */
public class Sistema {
    
    static Scanner tecla = new Scanner(System.in);
    static Aluno[] alunos = new Aluno[20];
    static int index = 0;
    
    public static void main(String[] args){
        
        int op;
        
        do {
            System.out.println("\n\tSistema de Controle de Notas");
            System.out.println("1 - Inserir Novo Aluno");
            System.out.println("2 - Excluir Aluno");
            System.out.println("3 - Lannçar/Editar Notas");
            System.out.println("4 - Consultar situação dos Alunos");
            System.out.println("5 - Sair");
            
            System.out.println("Digite a Operação Desejada: ");
            op = tecla.nextInt();
            
            switch(op){
                case 1: inserirAluno(); break;
                case 2: excluirAluno(); break;
                case 3: lancarEditar(); break;
                case 4: situacaoAl(); break;
                case 5: break;
                
                
            }
            
        } while(op != 5);
        
    }
    
    public static void inserirAluno(){
        System.out.println("Digite o nome do Aluno: ");
        String nome = tecla.next();
        System.out.println("Digite a Matrícula do Aluno: ");
        int mat = tecla.nextInt();
        alunos[index++] = new Aluno(nome, mat);
    }
    
    public static void excluirAluno(){
        System.out.println("Insira a matrícula do aluno à ser removido: ");
        int mat = tecla.nextInt();
        for(int i = 0; i < alunos.length - 1; i++){
            if(alunos[i] != null){
                if(alunos[i].matricula == mat){
                    alunos[i] = null;
                }
            }
        }
    }
    
    public static void lancarEditar(){
        System.out.println("Insira a Matrícula: ");
        int mat = tecla.nextInt();
        System.out.println("Insira a Nota do Aluno: ");
        double nota = tecla.nextDouble();
        
        for(int i = 0; i < alunos.length - 1; i++){
            if(alunos[i] != null){
                if(alunos[i].matricula == mat){
                    alunos[i].setNota(nota);
                }
            }
        }
    }
    
    public static void situacaoAl(){
        double media = 0;
        double qntAl = 0;
        String status;
        System.out.println("Digite 1 para listar Todos Alunos ou 2 para Consultar apenas um Aluno: ");
        int op = tecla.nextInt();
        
        if(op == 1){
            for(int i = 0; i < alunos.length - 1; i++){
                if(alunos[i] != null){
                    media += alunos[i].getNota();
                    qntAl += 1;
                    if(alunos[i].getNota() < 5){
                        status = "Reprovado";
                    }
                    else {
                        status = "Aprovado";
                    }
                    System.out.println(alunos[i].matricula + " - " + alunos[i].nome + " Nota - " + alunos[i].getNota() + " " + status);
                }
            }
            
            media /= qntAl;
            System.out.println("Média dos Alunos = " + media);
        }
        
        if(op == 2){
            System.out.println("\nDigite a Matrícula do Aluno para ser Consultado: ");
            int mat = tecla.nextInt();
            
            for(int i = 0; i < alunos.length - 1; i++){
                if(alunos[i] != null){
                    if(alunos[i].matricula == mat){
                        if(alunos[i].getNota() < 5){
                        status = "Reprovado";
                        }
                        else {
                            status = "Aprovado";
                        }
                        System.out.println(alunos[i].matricula + " - " + alunos[i].nome + "Nota - " + alunos[i].getNota() + " " + status);
                    }
                }
            }
            
        }
    }
    
}
