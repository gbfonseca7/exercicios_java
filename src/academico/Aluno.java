/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package academico;

/**
 *
 * @author gabriel
 */
public class Aluno {
   String nome;
   int matricula;
   private double nota;
   
   public Aluno(String n, int mat){
       this.nome = n;
       this.matricula = mat;
   }
   
   public void setNota(double nota){
       this.nota = nota;
   }
   public double getNota(){
       return this.nota;
   }
}
